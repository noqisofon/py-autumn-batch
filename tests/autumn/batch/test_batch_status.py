# -*- coding: utf-8; -*-
import unittest

from autumn.batch.core import BatchStatus


class BatchStatusTests(unittest.TestCase):

    def test_to_string(self):
        self.assertEqual( 'ABANDONED'            , BatchStatus.ABANDONED.name )
        self.assertEqual( 'BatchStatus.ABANDONED', str( BatchStatus.ABANDONED ) )

    def test_max_status(self):
        self.assertEqual( BatchStatus.FAILED , BatchStatus.max( BatchStatus.FAILED    , BatchStatus.COMPLETED ) )
        self.assertEqual( BatchStatus.FAILED , BatchStatus.max( BatchStatus.COMPLETED , BatchStatus.FAILED ) )
        self.assertEqual( BatchStatus.FAILED , BatchStatus.max( BatchStatus.FAILED    , BatchStatus.FAILED ) )

        self.assertEqual( BatchStatus.STARTED, BatchStatus.max( BatchStatus.STARTED   , BatchStatus.COMPLETED ) )
        self.assertEqual( BatchStatus.STARTED, BatchStatus.max( BatchStatus.COMPLETED , BatchStatus.STARTED ) )

    def test_upgrade_status_finished(self):
        self.assertEqual( BatchStatus.FAILED , BatchStatus.FAILED.upgrade_to( BatchStatus.COMPLETED ) )
        self.assertEqual( BatchStatus.FAILED , BatchStatus.COMPLETED.upgrade_to( BatchStatus.FAILED ) )

    def test_upgrade_upgrade_unfinish(self):
        self.assertEqual( BatchStatus.COMPLETED, BatchStatus.STARTING.upgrade_to( BatchStatus.COMPLETED ) )
        self.assertEqual( BatchStatus.COMPLETED, BatchStatus.COMPLETED.upgrade_to( BatchStatus.STARTING ) )
        self.assertEqual( BatchStatus.STARTED  , BatchStatus.STARTING.upgrade_to( BatchStatus.STARTED ) )
        self.assertEqual( BatchStatus.STARTED  , BatchStatus.STARTED.upgrade_to( BatchStatus.STARTING ) )

    def test_is_running(self):
        self.assertFalse( BatchStatus.FAILED.is_running() )
        self.assertFalse( BatchStatus.COMPLETED.is_running() )
        self.assertTrue( BatchStatus.STARTED.is_running() )
        self.assertTrue( BatchStatus.STARTING.is_running() )

    def test_is_unsuccessful(self):
        self.assertTrue( BatchStatus.FAILED.is_unsuccessful() )
        self.assertFalse( BatchStatus.COMPLETED.is_unsuccessful() )
        self.assertFalse( BatchStatus.STARTED.is_unsuccessful() )
        self.assertFalse( BatchStatus.STARTING.is_unsuccessful() )

    def test_get_status(self):
        self.assertEqual( BatchStatus.FAILED, BatchStatus.value_of( BatchStatus.FAILED.name ) )
        
if __name__ == '__main__':
    unittest.main()
