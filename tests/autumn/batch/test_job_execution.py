# -*- coding: utf-8; -*-
import unittest
import datetime

from autumn.batch.core import JobExecution, JobInstance, JobParameters


class JobExcecutionTests(unittest.TestCase):

    def setUp(self):
        self.__execution = JobExecution( JobInstance( 11, 'foo' ), 12, JobParameters() )

    def test_job_execution(self):
        an_execution = JobExecution( JobInstance( 11, 'foo' ), None, JobParameters() )

        self.assertTrue( an_execution.id is None )

    def test_get_end_time(self):
        self.assertTrue( self.__execution.end_time is None )
        self.__execution.end_time = datetime.datetime( 1990, 1, 2 )
        self.assertEqual( 1990, self.__execution.end_time.year )
        self.assertEqual(    1, self.__execution.end_time.month )
        self.assertEqual(    2, self.__execution.end_time.day )
        
if __name__ == '__main__':
    unittest.main()
