# -*- coding: utf-8; -*-
import unittest

from autumn.batch.core import Entity


class EntityTests(unittest.TestCase):

    def setUp(self):
        self.__entity = Entity( 11 )

    def test_hash_code(self):
        self.assertEqual( hash( self.__entity ), hash( Entity( self.__entity.id ) ) )

    def test_hash_code_null_id(self):
        without_null = hash( self.__entity )
        self.__entity.id = None
        with_null = hash( self.__entity )
        self.assertNotEqual( without_null, with_null )

    def test_get_version(self):
        self.assertTrue( self.__entity.version is None )

    def test_increment_version(self):
        self.__entity.increment_version()

        self.assertEqual( 0, self.__entity.version )

    def test_increment_version_twice(self):
        self.__entity.increment_version()
        self.__entity.increment_version()

        self.assertEqual( 1, self.__entity.version )

    def test_to_string(self):
        job = Entity()

        self.assertTrue( str( job ).index( 'id=None' ) >= 0 )

    def test_equals_self(self):
        self.assertEqual( self.__entity, self.__entity )

    def test_equals_self_with_null_id(self):
        self.__entity = Entity( None )

        self.assertEqual( self.__entity, self.__entity )

    def test_equals_entity_with_null_id(self):
        self.__entity = Entity( None )

        self.assertNotEqual( self.__entity, Entity( None ) )

    def test_equals_entity(self):
        self.assertEqual( self.__entity, Entity( self.__entity.id ) )

    def test_equals_entity_wrong_id(self):
        self.assertFalse( self.__entity == Entity() )

    def test_equals_object(self):
        self.assertNotEqual( self.__entity, object() )

    def test_equals_none(self):
        self.assertNotEqual( self.__entity, None )
        
if __name__ == '__main__':
    unittest.main()

