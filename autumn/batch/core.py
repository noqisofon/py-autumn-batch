# -*- coding: utf-8; -*-
import abc
import enum


class BatchStatus(enum.Enum):
    """
    """

    COMPLETED = enum.auto()
    """
    """

    STARTING = enum.auto()
    """
    """

    STARTED = enum.auto()
    """
    """

    STOPPING = enum.auto()
    """
    """

    FAILED = enum.auto()
    """
    """

    ABANDONED = enum.auto()
    """
    """

    UNKNOWN = enum.auto()
    """
    """

    def is_running(self):
        """
        レシーバが起動、起動中を意味するステータスであれば真を返します。

        Returns
        -------
        bool
            起動、起動中を意味するステータスであれば真。
        """
        return self == BatchStatus.STARTING or self == BatchStatus.STARTED

    def is_unsuccessful(self):
        """
        """
        return self == BatchStatus.FAILED or self.is_greater_than( BatchStatus.FAILED )
        
    def is_greater_than(self, other):
        """
        """
        return self.value > other.value

    def upgrade_to(self, other):
        """
        """
        if self.is_greater_than( BatchStatus.STARTED ) or other.is_greater_than( BatchStatus.STARTED ):
            return BatchStatus.max( self, other )

        if self == BatchStatus.COMPLETED or other == BatchStatus.COMPLETED:
            return BatchStatus.COMPLETED

        return BatchStatus.max( self, other )

    @staticmethod
    def max(status1, status2):
        """
        """
        if status1.is_greater_than( status2 ):
            return status1
        else:
            return status2

    @staticmethod
    def value_of(value):
        """
        """
        for name, member in BatchStatus.__members__.items():
            if name.upper() == value.upper():
                return member
        return None

class Entity:
    """
    """
    def __init__(self, entity_id=None, version=None):
        """
        """
        self.__id = entity_id
        self.__version = version

    @property
    def id(self):
        """
        """
        return self.__id

    @id.setter
    def id(self, value):
        """
        """
        self.__id = value

    @property
    def version(self):
        """
        """
        return self.__version

    @version.setter
    def version(self, value):
        """
        """
        self.__version = value

    def increment_version(self):
        """
        """
        if self.__version is None:
            self.__version = 0
        else:
            self.__version += 1

    def __str__(self):
        return '{}: id={}, version={}'.format( 
                self.__class__.__name__, 
                self.__id, 
                self.__version )

    def __eq__(self, other):
        if other is None:
            return False
        if other is self:
            return True
        if not isinstance( other, Entity ):
            return False
        if self.__id is None or other.__id is None:
            return False
        return self.__id == other.__id
    
    def __hash__(self):
        if self.__id is None:
            return super().__hash__()
        return 39 + 87 * hash( self.__id )

class Job(metaclass=abc.ABCMeta):
    """
    """

    @abc.abstractmethod
    def is_restartable(self):
        """
        このジョブが再起動できるジョブかどうか判別します。

        Returns
        -------
        bool
            再起動できるジョブなら真。
        """
        pass

    @abc.abstractmethod
    def execute(self, execution):
        """
        

        Parameters
        ----------
        execution : JobExecution
        """
        pass

class JobExecution(Entity):
    """
    """

    def __init__(self, a_job, execution_id=None, parameters=None, configuration_name=None, start_time=None, end_time=None, status=BatchStatus.UNKNOWN):
        """

        Parameters
        ----------
        a_job : JobInstance

        execution_id : Int

        parameters : JobParameters

        configuration_name : str

        start_time : datetime.datetime

        end_time : datetime.datetime

        status : BatchStatus
        """
        super().__init__( execution_id )
        self.__instance = a_job
        if parameters is None:
            self.__parameters = JobParameters()
        else:    
            self.__parameters = parameters
        self.__configuration_name = configuration_name
        self.__start_time = start_time
        self.__end_time = None
        self.__status = status

    @property
    def parameters(self):
        return self.__parameters

    @property
    def start_time(self):
        return self.__start_time

    @start_time.setter
    def start_time(self, value):
        self.__start_time = value

    @property
    def end_time(self):
        return self.__end_time

    @end_time.setter
    def end_time(self, value):
        self.__end_time = value

class JobInstance(Entity):
    """
    """

    def __init__(self, job_instance_id, name):
        """
        """
        super().__init__( job_instance_id )
        assert len( name ) > 0
        self.__name = name

class JobParameters:
    """

    Nones
    -----
    Python なので、型付き get や set は実装しない。
    """

    def __init__(self):
        self.__parameters = {}

    def is_empty(self):
        """
        """
        return len( self.__parameters ) == 0
